# APIREST BBVA TECH

Este API REST está diseñado para el proyecto final del programa BBVA Tech University
Todo el código está alojado en Bitbucket en el siguiente repositorio: 

### Repositorio

[Repositorio!](https://bitbucket.org/krica/proyectotechu2020/src/master/)

### IntroducciÓn
Este API está desarrollado para una interfaz web de una banca Móvil en el que tendremos inicio de sesión (Entrar y salir), mantenimiento de usuarios, mantenimiento de cuentas y mantenimiento de movimientos. Contemplamos distintos casos de uso que están descritos más abajo seguidos de su ruta para probarlos.
Para todos los métodos de GET, POST, PUT y DELETE tendremos que pasarle un JSON con los campos necesarios.

### IntroducciÓn
GET: 
Usuarios
api/usuarios/:login -> Compara el email y la contraseña ingresada, si coinciden el usuario estará logueado.
api/usuarios/id - >Obtiene un usuario en específico.
api/usuarios -> Lista los usuarios registrados.

Cuentas
api/cuenta/:id -> Devuelve la cuenta con el ID indicado.
api/cuentas -> Lista todas las cuentas.

Movimientos
api/movimientos/cuenta ->Devuelve todos los movimientos de una cuenta específica


POST: 
Usuarios
api/usuarios/nuevo -> A partir de un JSON que se le pasa añade un nuevo usuario a la base de datos.

api/movimiento/nuevo -> A partir de un JSON que se le pasa añade un nuevo movimiento a la base de datos.

api/login -> A partir de un JSON se comprueba que un usuario cumple con la condición.

api/logout -> A partir de un JSON se comprueba que un usuario cumple con la condición.


DELETE
Usuarios
api/usuarios/:id -> Borramos el usuario usando su id respectivo.
Movimientos
api/movimiento/:id -> Borramos movimiento a partir de su ID.
PUT
Usuarios
api/usuarios/:id -> Editamos al usuario que indicamos en el login a través de un JSON con sus campos, Los datos para modificar se pasaran por JSON.


##### Lista de comandos para puesta en marcha

Para poner en marcha la aplicación tendremos que realizar los siguientes comandos:
En el backend
npm install //con esto instalaremos los módulos necesarios para node.js
Creamos las collections de usuario, cuenta y movimiento en la BBDD MongoD
usando mlab. 
[https://api.mlab.com/api/1/databases/techu18db/collections/user?apiKey=NQCR6_EMDAdqyM6VEWg3scF_k32uwvHF!](https://api.mlab.com/api/1/databases/techu18db/collections/user?apiKey=NQCR6_EMDAdqyM6VEWg3scF_k32uwvHF/)

https://api.mlab.com/api/1/databases/techu18db/collections/user?apiKey=NQCR6_EMDAdqyM6VEWg3scF_k32uwvHF
 
Desde el backend accedemos a la BD que está alojada en mLab.
 
Usaremos en npm run dev o npm run prod para no estar ejecutando el servidor cada vez que hagamos un cambio.
 
Para realizar las pruebas usaremos una aplicación llamada PostMan, esta aplicación permite realizar las peticiones necesarias para un API REST (GET, POST, DELETE, PUT).

### Autores

KRISS MINANO ROSAS - VANEZA ORTIZ LOBATO
