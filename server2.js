require('dotenv').config();
const express= require ('express');
const body_parser= require('body-parser');
const request_json= require('request-json');
const cors = require('cors');  // Instalar dependencia 'cors' con npm
const app = express();
const port = process.env.PORT || 3000;
//const URL_BASE = '/tech/v1/';
const URL_BASE = '/tech/v2/';

//const usersFile = require('./user.json');
//const movimientoFile = require('./movimiento.json');
//const URL_BASE = '/techu/v2';
const URL_mlap='https://api.mlab.com/api/1/databases/techu18db/collections/';
const apikey_mlab='apiKey='+process.env.API_KEY_MLAB;
var newID=0;
app.listen(port, function(){
console.log('Node esta escuchando: ' + port);
});

app.use(cors());
app.options('*', cors());

app.use(body_parser.json());

//operacion get Colleccion mlab
app.get(URL_BASE+ 'users',
	function(req, res){
    const http_client= request_json.createClient(URL_mlap);
    let field_param= 'f={"_id":0}&';
    console.log('cliente http');
    console.log('user?'+field_param+apikey_mlab);
    http_client.get('user?'+field_param+apikey_mlab,
    function(error, res_mlab, body) {
      console.log('Error: ' + error);
      console.log('Respuesta MLab: ' + res_mlab);
      console.log('Body: ' + body);
      var response = {};
      if(error) {
          response = {"msg" : "Error al recuperar users de mLab."}
          res.status(500);
      } else {
        if(body.length > 0) {
          response = body;
        } else {
          response = {"msg" : "Usuario no encontrado."};
          res.status(404);
        }
      }
      res.send(response);
    });
	//response.status(200).send({'msg':'cliente http'});
});

//GET users con ID(mLab)
app.get(URL_BASE+'users/:id',
	function(req, res){
  console.log(req.params.id);
  const httpClient= request_json.createClient(URL_mlap);
  let id = req.params.id;
  // let queryString= 'q={"id":'+id+'}&';
  //se usa el simbolo template string `` y en vez de mas se usa ${}
  let queryString= `q={"id":${id}}&`;
  let field_param= 'f={"_id":0}&';
  httpClient.get('user?'+queryString+field_param+apikey_mlab,
  function(error,res_mlab,body){
      let response ={};
      if (error){
        response = {'msg':'Error en la petición a mLab'};
        res.status(500);
      }else{
        if(body.length>0){
          response=body;

        }else{
          response = {'msg':'Usuario no encontrado'};
          res.status(404);
        }

      }
      res.send(response);
    });

});

//Listar todas las cuentas

app.get(URL_BASE+ 'accounts',
  function(req, res){
    console.log('GET accounts');
    const httpClient= request_json.createClient(URL_mlap);
   // let id = req.params.id;
   // let queryString= `q={"id_user":${id}}&`;
    //let field_param= 'f={"account":1,"_id":0}&';
    httpClient.get('account?'+apikey_mlab,
    function(error,res_mlab,body){
        let response ={};
        if (error){
          response = {'msg':'Error en la petición a mLab'};
          res.status(500);
        }else{
          if(body.length>0){
            response=body;

          }else{
            response = {'msg':'Cuenta no encontrada'};
            res.status(404);
          }

        }
        res.send(response);
      });
  });

//Listar las cuentas por usuario

app.get(URL_BASE+ 'accounts/:id',
  function(req, res){
    console.log('GET accounts/:id');
    const httpClient= request_json.createClient(URL_mlap);
    let id = req.params.id;
    let queryString= `q={"id_user":${id}}&`;
    //let field_param= 'f={"account":1,"_id":0}&';
    httpClient.get('account?'+queryString+apikey_mlab,
    function(error,res_mlab,body){
        let response ={};
        if (error){
          response = {'msg':'Error en la petición a mLab'};
          res.status(500);
        }else{
          if(body.length>0){
            response=body;

          }else{
            response = {'msg':'Cuenta no encontrada'};
            res.status(404);
          }

        }
        res.send(response);
      });
  });
//Crear movimiento POST movimiento
  app.post(URL_BASE+ "movimientos",
function(req, res) {
 var  clienteMlab = request_json.createClient(URL_mlap);
 clienteMlab.get('movimiento?'+ apikey_mlab ,
 function(error, respuestaMLab , body) {
     newID=body.length+1;
     console.log("newID:" + newID);
     var newMovimiento = {
       "id_movimiento" : newID+1,
       "id_account" : req.body.id_account,
       "descripcion" : req.body.descripcion,
       "fecha" : req.body.fecha,
       "monto" : req.body.monto
     };
     clienteMlab.post(URL_mlap + "movimiento?" + apikey_mlab, newMovimiento ,
      function(error, respuestaMLab, body) {
       res.send(body);
    });
 });
});

  //Listar los movimientos de las cuentas

app.get(URL_BASE+ 'movimiento/:id_account',
  function(req, res){
    console.log('GET movimientos/:id_account');
    const httpClient= request_json.createClient(URL_mlap);
    let id_account = req.params.id_account;
    let queryString= `q={"id_account":${id_account}}&`;
    //let field_param= 'f={"account":1,"_id":0}&';
    httpClient.get('movimiento?'+queryString+apikey_mlab,
    function(error,res_mlab,body){
        let response ={};
        if (error){
          response = {'msg':'Error en la petición a mLab'};
          res.status(500);
        }else{
          if(body.length>0){
            response=body;

          }else{
            response = {'msg':'Movimiento no encontrado'};
            res.status(404);
          }

        }
        res.send(response);
      });
  });

	//Eliminar Movimientos de una cuenta especifica

	//DELETE  id_movimiento
	app.delete(URL_BASE + "movimiento/:id_movimiento",
	  function(req, res){
	    console.log("entra al DELETE");
	    console.log("request.params.id_movimiento: " + req.params.id_movimiento);
	    var id_movimiento = req.params.id_movimiento;
	    var queryStringID = 'q={"id_movimiento":' + id_movimiento + '}&';
	    console.log(URL_mlap + 'movimiento?' + queryStringID + apikey_mlab);
	    var httpClient = request_json.createClient(URL_mlap);
	    httpClient.get('movimiento?' +  queryStringID +  apikey_mlab,
	      function(error, respuestaMLab, body){
	        var respuesta = body[0];
	        httpClient.delete(URL_mlap + "movimiento/" + respuesta._id.$oid +'?'+ apikey_mlab,
	          function(error, respuestaMLab,body){
	            res.send(body);
	        });
	      });
	  });

//GET cuentas del usuario con párametro ID
app.get(URL_BASE+ 'users/:id/accounts',
  function(req, res){
    console.log('GET users/:id/accounts');
    const httpClient= request_json.createClient(URL_mlap);
    let id = req.params.id;
    let queryString= `q={"id":${id}}&`;
    let field_param= 'f={"account":1,"_id":0}&';
    httpClient.get('user?'+queryString+field_param+apikey_mlab,
    function(error,res_mlab,body){
        let response ={};
        if (error){
          response = {'msg':'Error en la petición a mLab'};
          res.status(500);
        }else{
          if(body.length>0){
            response=body;

          }else{
            response = {'msg':'Usuario no encontrado'};
            res.status(404);
          }

        }
        res.send(response);
      });
  });

//POST of user
app.post(URL_BASE+ "users",
function(req, res) {
 var  clienteMlab = request_json.createClient(URL_mlap);
 clienteMlab.get('user?'+ apikey_mlab ,
 function(error, respuestaMLab , body) {
     newID=body.length+1;
     console.log("newID:" + newID);
     var newUser = {
       "id" : newID+1,
       "first_name" : req.body.first_name,
       "last_name" : req.body.last_name,
       "email" : req.body.email,
       "password" : req.body.password
     };
     clienteMlab.post(URL_mlap + "user?" + apikey_mlab, newUser ,
      function(error, respuestaMLab, body) {
       res.send(body);
    });
 });
});
//put
app.put(URL_BASE + 'users2/:id',
function(req, res) {
  var id = req.params.id;
  var queryStringID = 'q={"id":' + id + '}&';
  var clienteMlab = request_json.createClient(URL_mlap);
  clienteMlab.get('user?'+ queryStringID + apikey_mlab,
    function(error, respuestaMLab, body) {
     var cambio = '{"$set":' + JSON.stringify(req.body) + '}';
     console.log(req.body);
     console.log(cambio);
     clienteMlab.put(URL_mlap+'user?' + queryStringID + apikey_mlab, JSON.parse(cambio),
      function(error, respuestaMLab, body) {
        console.log("body:"+ body); // body.n == 1 si se pudo hacer el update
       //res.status(200).send(body);
       res.send(body);
      });
    });
});

// Petición PUT con id de mLab (_id.$oid)
app.put(URL_BASE + 'users/:id',
function (req, res) {
  var id = req.params.id;
  let userBody = req.body;
  var queryString = 'q={"id":' + id + '}&';
  var httpClient = request_json.createClient(URL_mlap);
  httpClient.get('user?' + queryString + apikey_mlab,
    function(err, respuestaMLab, body){
      let response = body[0];
      console.log(body);
      //Actualizo campos del usuario
      let updatedUser = {
        "id" : req.body.id,
        "first_name" : req.body.first_name,
        "last_name" : req.body.last_name,
        "email" : req.body.email,
        "password" : req.body.password
      };//Otra forma optimizada (para muchas propiedades)
      // var updatedUser = {};
      // Object.keys(response).forEach(key => updatedUser[key] = response[key]);
      // Object.keys(userBody).forEach(key => updatedUser[key] = userBody[key]);
      // PUT a mLab
      httpClient.put('user/' + response._id.$oid + '?' + apikey_mlab, updatedUser,
        function(err, respuestaMLab, body){
          var response = {};
          if(err) {
              response = {
                "msg" : "Error actualizando usuario."
              }
              res.status(500);
          } else {
            if(body.length > 0) {
              response = body;
            } else {
              response = {
                "msg" : "Usuario actualizado correctamente."
              }
              res.status(404);
            }
          }
          res.send(response);
        });
    });
});

//DELETE user with id
app.delete(URL_BASE + "users/:id",
  function(req, res){
    console.log("entra al DELETE");
    console.log("request.params.id: " + req.params.id);
    var id = req.params.id;
    var queryStringID = 'q={"id":' + id + '}&';
    console.log(URL_mlap + 'user?' + queryStringID + apikey_mlab);
    var httpClient = request_json.createClient(URL_mlap);
    httpClient.get('user?' +  queryStringID + apikey_mlab,
      function(error, respuestaMLab, body){
        var respuesta = body[0];
        console.log("body delete:"+ JSON.stringify(respuesta));
        httpClient.delete(URL_mlap + "user/" + respuesta._id.$oid +'?'+ apikey_mlab,
          function(error, respuestaMLab,body){
            res.send(body);
        });
      });
  });
// DELETE con PUT de mLab
app.delete(URL_BASE + "users1/:id",
  function(req, res){
    var id = Number.parseInt(req.params.id);
    var query = 'q={"id":' + id + '}&';
    // Interpolación de expresiones
    var query2 = `q={"id":${id} }&`;
    var query3 = "q=" + JSON.stringify({"id": id})
 + '&';
    httpClient = request_json.createClient(URL_mlap);
    httpClient.put("user?" + query + apikey_mlab, [{}],
        function(err, resMLab, body) {
          var response = !err ? body : {
            "msg" : "Error borrando usuario."
          }
        res.send(response);
      }
    );
  });

  app.put(URL_BASE + "users1/:id",
  function(req, res){
    var id = Number.parseInt(req.params.id);
    var query = 'q={"id":' + id + '}&';
    // Interpolación de expresiones
    var query2 = `q={"id":${id} }&`;
    var query3 = "q=" + JSON.stringify({"id": id})
 + '&';
    httpClient = request_json.createClient(URL_mlap);
    httpClient.put("user?" + query + apikey_mlab, [{}],
        function(err, resMLab, body) {
          var response = !err ? body : {
            "msg" : "Error borrando usuario."
          }
        res.send(response);
      }
    );
  });


app.delete(URL_BASE+'users/:id',
function(req,res){
 console.log('POST a users');
 let pos= req.params.id - 1;
 usersFile.splice(pos, 1);
 res.send({"msg":"Usuario eliminado correctamente:"+ req.params.id });
});

// Petición GET con Query String (req.query)
app.get(URL_BASE + 'users',
  function(req, res) {
    console.log("GET con query string.");
    console.log(req.query.id);
    console.log(req.query.country);
    res.send(usersFile[pos - 1]);
    respuesta.send({"msg" : "GET con query string"});
});

//Method POST login mlab
app.post(URL_BASE + "login1",
  function (req, res){
    console.log("POST /tech/v2/login");
    let email = req.body.email;
    let pass = req.body.password;
    let queryString = 'q={"email":"' + email + '","password":"' + pass + '"}&';
    let limFilter = 'l=1&';
    let clienteMlab = request_json.createClient(URL_mlap);
    clienteMlab.get('user?'+ queryString + limFilter + apikey_mlab,
      function(error, respuestaMLab, body) {
        if(!error) {
          if (body.length == 1) { // Existe un usuario que cumple 'queryString'
            let login = '{"$set":{"logged":true}}';
            clienteMlab.put('user?q={"id": ' + body[0].id + '}&' + apikey_mlab, JSON.parse(login),
            //clienteMlab.put('user/' + body[0]._id.$oid + '?' + apikeyMLab, JSON.parse(login),
              function(errPut, resPut, bodyPut) {
                res.send({'msg':'Login correcto', 'user':body[0].email, 'userid':body[0].id, 'name':body[0].first_name});
                // If bodyPut.n == 1, put de mLab correcto
              });
          }
          else {
            res.status(404).send({"msg":"Usuario no válido."});
          }
        } else {
          res.status(500).send({"msg": "Error en petición a mLab."});
        }
    });
});
//ENTREGABLE3- Method POST login
app.post(URL_BASE + "login",
  function (req, res){
    console.log("POST /tech/v2/login");
    var email= req.body.email;
    var pass= req.body.password;
    var queryStringEmail='q={"email":"' + email + '"}&';
    var queryStringpass='q={"password":' + pass + '}&';
    var  clienteMlab = request_json.createClient(URL_mlap);
    clienteMlab.get('user?'+ queryStringEmail+apikey_mlab ,
    function(error, respuestaMLab , body) {
      console.log("entro al body:" + body );
      var respuesta = body[0];
      console.log(respuesta);
      if(respuesta!=undefined){
          if (respuesta.password == pass) {
            console.log("Login Correcto2");
            var session={"logged":true};
            var login = '{"$set":' + JSON.stringify(session) + '}';
            console.log(URL_mlap+'?q={"id": ' + respuesta.id + '}&' + apikey_mlab);
            clienteMlab.put('user?q={"id": ' + respuesta.id + '}&' + apikey_mlab, JSON.parse(login),
             function(errorP, respuestaMLabP, bodyP) {
              //res.send(body[0]);
              console.log("Login correcto!");
              res.status(200).send({'msg':'Login correcto', 'userid':respuesta.id, 'email':respuesta.email,  'name':respuesta.first_name, 'logged':true });
            });
          }
          else {
            console.log("Contraseña incorrecto.");
            res.send({"msg":"contraseña incorrecta","logged": false});
          }
      }else{
        console.log("Email Incorrecto");
        res.status(404).send({"msg": "email Incorrecto","logged": false});
      }
    });
});



//Method POST logout
app.post(URL_BASE + "logout2",
  function (req, res){
    console.log("POST /tech/v2/logout2");
    var email= req.body.email;
    var queryStringEmail='q={"email":"' + email + '"}&';
    var  clienteMlab = request_json.createClient(URL_mlap);
    clienteMlab.get('user?'+ queryStringEmail+apikey_mlab ,
    function(error, respuestaMLab , body) {
      console.log("entro al get");
      var respuesta = body[0];
      console.log(respuesta);
      if(respuesta!=undefined){
            console.log("logout Correcto");
            var session={"logged":true};
            var logout = '{"$unset":' + JSON.stringify(session) + '}';
            console.log(logout);
            clienteMlab.put('user?q={"id": ' + respuesta.id + '}&' + apikey_mlab, JSON.parse(logout),
             function(errorP, respuestaMLabP, bodyP) {
             // res.send(body[0]);
             console.log("Logout correcto!");
              res.send({'msg':'Logout correcto', 'user':respuesta.email});
            });
      }else{
        console.log("Logout incorrecto.");
        response.send({"msg" : "Logout incorrecto."});
      }
    });
});
//ENTREGABLE 3- Method POST logout
app.post(URL_BASE + "logout",
  function(req, res) {
    console.log("POST POST /tech/v2/logout");
    var email = req.body.email;
    var queryString = 'q={"email":"' + email + '","logged":true}&';
    console.log(queryString);
    var  clienteMlab = request_json.createClient(URL_mlap);
    clienteMlab.get('user?'+ queryString + apikey_mlab,
      function(error, respuestaMLab, body) {
        var respuesta = body[0]; // Asegurar único usuario
        if(!error) {
          if (respuesta != undefined) { // Existe un usuario que cumple 'queryString'
            let logout = '{"$unset":{"logged":true}}';
            clienteMlab.put('user?q={"id": ' + respuesta.id + '}&' + apikey_mlab, JSON.parse(logout),
            //clienteMlab.put('user/' + respuesta._id.$oid + '?' + apikeyMLab, JSON.parse(logout),
              function(errPut, resPut, bodyPut) {
             console.log("Logout correcto!");
             res.send({'msg':'Logout correcto', 'user':respuesta.email});
                // If bodyPut.n == 1, put de mLab correcto
              });
            } else {
              console.log("Logout incorrecto.");
                res.status(404).send({"msg":"Logout incorrecto."});
            }
        } else {
          res.status(500).send({"msg": "Error en petición a mLab."});
        }
    });
});


//Petición GET RETO2 total de usuarios
  app.get(URL_BASE+'total',
    function(request, response){
      total = usersFile.length;
     response.send({"numeros de usuarios: ": total });
  });
