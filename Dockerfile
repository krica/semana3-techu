#Imagen docker base inicial
FROM node:latest
#crear directorio de trabajo del contenedor docker
WORKDIR /docker-dir-apitechu
#copiar archvis del proeuycto en el directoria trabajo docker
ADD . /docker-dir-apitechu
#PUERTO DONDE EXPONEMOS CONETENEDOR
EXPOSE 3000
#COMANDO  PARA LANZAR APP
CMD ["npm","run","prod"]

#sudo docker build -t api:v1 .
#sudo docker ps
#docker run -p 3000:3000 api:v1
# port1:local port2:contenedor